#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream> 
#include <iomanip> 
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */

class CPersonalAgenda
{
  struct Salary;
  struct Email;
  struct Employee{
    string name;
    string surname;
    Email * email;
    Salary * salary;
    Employee * parent;
    Employee * left;
    Employee * right;
  };

  struct Email{
    string email;
    Employee * owner;
    Email * parent;
    Email * left;
    Email * right;
  };

  struct Salary{
    unsigned int salary;
    Employee * owner;
    Salary * parent;
    Salary * left;
    Salary * right;
  };

  Employee * empRoot;
  Email * emailRoot;
  Salary * salaryRoot;

  public:
                  CPersonalAgenda  ( void );
    void          destroyRecursive ( Employee       *& node );
    void          destroyRecursive ( Email          *& node );
    void          destroyRecursive ( Salary         *& node);
                  ~CPersonalAgenda ( void );
    bool          findByName       ( const string    & name,
                                     const string    & surname,
                                     Employee       *& node,
                                     Employee       *& parent) const;
    bool          findByEmail      ( const string    & email,
                                     Email          *& node,
                                     Email          *& parent ) const;
    void          insertSalary     ( Salary         *& node, 
                                     Salary         *  newSalary,
                                     Salary         *  parent);
    bool          insertEmail      ( Email          *& node, 
                                     Email          *  newEmail,
                                     Email          *  parent);                                 
    bool          insertEmployee   ( Employee       *& node, 
                                     Employee       *  newEmployee,
                                     Employee       *  parent,
                                     Email          *  newEmail);
    bool          add              ( const string    & name,
                                     const string    & surname,
                                     const string    & email,
                                     unsigned int      salary );
    void          deleteEmpNode    ( Employee       *  nodeToDelete );
    void          deleteEmailNode  ( Email          *  nodeToDelete );
    void          deleteSalaryNode ( Salary         *  nodeToDelete );
    bool          del              ( const string    & name,
                                     const string    & surname );
    bool          del              ( const string    & email );
    bool          changeName       ( const string    & email,
                                     const string    & newName,
                                     const string    & newSurname );
    bool          changeEmail      ( const string    & name,
                                     const string    & surname,
                                     const string    & newEmail );
    bool          setSalary        ( const string    & name,
                                     const string    & surname,
                                     unsigned int      salary );
    bool          setSalary        ( const string    & email,
                                     unsigned int      salary );
    unsigned int  getSalary        ( const string    & name,
                                     const string    & surname ) const;
    unsigned int  getSalary        ( const string    & email ) const;
    void          traverseTree     ( Salary         *  node, 
                                     unsigned int      mySalary, 
                                     int             & low, 
                                     int             & high ) const;
    bool          getRank          ( const string    & name,
                                     const string    & surname,
                                     int             & rankMin,
                                     int             & rankMax ) const;
    bool          getRank          ( const string    & email,
                                     int             & rankMin,
                                     int             & rankMax ) const;
    bool          getFirst         ( string          & outName,
                                     string          & outSurname ) const;
    int           compareEmployees (const string     & name,
                                    const string     & surname,
                                    Employee        *& emp ) const;
    bool          getNext          ( const string    & name,
                                     const string    & surname,
                                     string          & outName,
                                     string          & outSurname ) const;
};

CPersonalAgenda::CPersonalAgenda() : empRoot(nullptr), emailRoot(nullptr), salaryRoot(nullptr) {}

void CPersonalAgenda::destroyRecursive(Employee *& node)
{
  if (node){
    destroyRecursive(node->left);
    destroyRecursive(node->right);
    delete node;
  }
}
void CPersonalAgenda::destroyRecursive(Email *& node)
{
  if (node){
    destroyRecursive(node->left);
    destroyRecursive(node->right);
    delete node;
  }
}

void CPersonalAgenda::destroyRecursive(Salary *& node)
{
  if (node){
    destroyRecursive(node->left);
    destroyRecursive(node->right);
    delete node;
  }
}

CPersonalAgenda::~CPersonalAgenda()
{
    destroyRecursive(empRoot);
    destroyRecursive(emailRoot);
    destroyRecursive(salaryRoot);
}
//************************************************************************************************************************************************

bool CPersonalAgenda::findByName( const string & name, const string & surname, Employee *& node, Employee *& parent ) const{
    // Traverse the tree to find the specified employee
    while (node != nullptr) {
        int cmp = compareEmployees(name, surname, node);
        if (cmp == 0) {
            break; // Found the specified employee
        } else if (cmp < 0) {
            // Specified employee is smaller, go left
            parent = node;
            node = node->left;
        } else {
            // Specified employee is larger, go right
            parent = node;
            node = node->right;
        }
    }
    // If the specified employee was not found, return false
    if (node == nullptr) {
        return false;
    }
    else return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::findByEmail(const string & email, Email *& node, Email *& parent ) const{
// Traverse the tree to find the specified employee
  while (node != nullptr) {
      int cmp = email.compare(node->email);
      if (cmp == 0) {
          break; // Found the specified employee
      } else if (cmp < 0) {
          // Specified employee is smaller, go left
          parent = node;
          node = node->left;
      } else {
          // Specified employee is larger, go right
          parent = node;
          node = node->right;
      }
  }
  // If the specified employee was not found, return false
  if (node == nullptr) {
      return false;
  }
  else return true;
}
//************************************************************************************************************************************************

void CPersonalAgenda::insertSalary(Salary *& node, Salary * newSalary, Salary * parent) 
{
  if (node == nullptr) {
      node = newSalary;
      node->parent = parent; // Set the parent of the new node
  } 
  else {
      int cmp = newSalary->salary - node->salary;   
      if (cmp < 0) 
          insertSalary(node->left, newSalary, node); // Pass in current node as parent
      else //if (cmp >= 0) 
          insertSalary(node->right, newSalary, node); // Pass in current node as parent
  }
}
//************************************************************************************************************************************************

bool CPersonalAgenda::insertEmail(Email *& node, Email * newEmail, Email * parent) 
{
  if (node == nullptr) {
      node = newEmail;
      node->parent = parent; // Set the parent of the new node
  } 
  else {
      int cmp = newEmail->email.compare(node->email);
      if( cmp == 0) // Found the same email
          return false;
      else if (cmp < 0) {
          if (!insertEmail(node->left, newEmail, node)) {
              return false; 
          }
      }
      else {
          if (!insertEmail(node->right, newEmail, node)) {
              return false; 
          }
      }
  }
  return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::insertEmployee(Employee *& node, Employee * newEmployee, Employee * parent, Email * newEmail) 
{
  if (node == nullptr) {
    if( !insertEmail(emailRoot, newEmail, nullptr) )
        return false;
    node = newEmployee;
    node->parent = parent; // Set the parent of the new node
  }
  else {
    int cmp = compareEmployees(newEmployee->name, newEmployee->surname, node);
    if( cmp == 0) // This employee is already in the tree
      return false;
    else if (cmp < 0){
        if( !insertEmployee(node->left, newEmployee, node, newEmail) ) // Pass in current node as parent
          return false;
    }
    else {//if (cmp > 0)
        if( !insertEmployee(node->right, newEmployee, node, newEmail) ) // Pass in current node as parent
          return false;
    }
  }
  return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::add ( const string & name, const string & surname, const string & email, unsigned int salary )
{
  Email * newEmail = new Email {email, nullptr, nullptr, nullptr, nullptr};
  Salary * newSalary = new Salary {salary, nullptr, nullptr, nullptr, nullptr};
  Employee * newEmployee = new Employee {name, surname, newEmail, newSalary, nullptr, nullptr, nullptr};

  if( !insertEmployee(empRoot, newEmployee, nullptr, newEmail ) ){
    delete newEmployee;
    delete newEmail;
    delete newSalary;
    return false;
  }
  insertSalary(salaryRoot, newSalary, nullptr);
  newEmail->owner = newEmployee;
  newSalary->owner = newEmployee;
  return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::getFirst ( string & outName, string & outSurname ) const
{
  if (empRoot == nullptr)
      return false;
  Employee* current = empRoot;
  // Loop to the most left element in the tree
  while (current->left != nullptr) 
      current = current->left;
  outName = current->name;
  outSurname = current->surname;
  return true;
}
//************************************************************************************************************************************************

int CPersonalAgenda::compareEmployees(const string & name, const string & surname, Employee *& emp) const
{
if (emp->surname != surname)
    return -(emp->surname.compare(surname));
else
    return -(emp->name.compare(name));
}
//************************************************************************************************************************************************

bool CPersonalAgenda::getNext(const string& name, const string& surname, string& outName, string& outSurname) const
{
    Employee * curr = empRoot;
    Employee * parent = empRoot;

    if( ! findByName( name, surname, curr, parent))
        return false;
    // If the employee has a right subtree, find the leftmost node in that subtree
    if (curr->right != nullptr) {
        Employee* next = curr->right;
        while (next->left != nullptr)
            next = next->left;

        outName = next->name;
        outSurname = next->surname;
        return true;
    }
    // If the employee does not have a right subtree, find the first ancestor that is a left child of its parent
    while (parent != nullptr && curr == parent->right) {
        curr = parent;
        parent = parent->parent;
    }
    // If there is no such ancestor, there is no next employee
    if (parent == nullptr || (parent != nullptr && parent->left != curr)) 
        return false;
    // Otherwise, the parent is the next employee
    outName = parent->name;
    outSurname = parent->surname;
    return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::setSalary( const string& name, const string& surname, unsigned int  salary ){
  Employee * employee = empRoot;
  Employee * dummyParent;
  if( !findByName( name, surname, employee, dummyParent ))
      return false;
  employee->salary->salary = salary;
  return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::setSalary( const string& email, unsigned int salary ){
  Email * emailPtr = emailRoot;
  Email * parent;
  if( ! findByEmail(email, emailPtr, parent))
      return false;
  emailPtr->owner->salary->salary = salary;
  return true;
}
//************************************************************************************************************************************************

unsigned int  CPersonalAgenda::getSalary( const string& name, const string& surname ) const{
  Employee * employee = empRoot;
  Employee * dummyParent;
  if( !findByName( name, surname, employee, dummyParent ))
      return 0;
  return employee->salary->salary;
} 
//************************************************************************************************************************************************

unsigned int  CPersonalAgenda::getSalary( const string& email ) const{
  Email * emailPtr = emailRoot;
  Email * parent;
  if( ! findByEmail(email, emailPtr, parent))
      return 0;
  return emailPtr->owner->salary->salary;
}
//************************************************************************************************************************************************

void CPersonalAgenda::traverseTree(Salary * node, unsigned int mySalary, int & low, int & high ) const
{
  // Loops through the tree, until salaries are the same both low and high is getting bigger, then only the high is getting bigger
  if (node == NULL)
      return;
  traverseTree(node->left, mySalary, low, high);
  if( node->salary < mySalary ){
    low++;
    high++;
  } else if( node->salary == mySalary){
    high++;
  }
  traverseTree(node->right, mySalary, low, high);
}
//************************************************************************************************************************************************

bool CPersonalAgenda::getRank ( const string& name, const string& surname, int& rankMin, int& rankMax ) const
{
  Employee * employee = empRoot;
  Employee * parent = nullptr;
  if( !findByName(name, surname, employee, parent) ) 
      return false;
  unsigned int mySalary = employee->salary->salary;
  rankMin = 0;
  rankMax = -1;
  traverseTree( salaryRoot, mySalary, rankMin, rankMax);
  return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::getRank ( const string& email, int& rankMin, int& rankMax ) const
{
  rankMin = 0;
  rankMax = -1;
  Email * emailPtr = emailRoot;
  Email * parent = nullptr;
  if( !findByEmail(email, emailPtr, parent) ) 
      return false;
  unsigned int mySalary = emailPtr->owner->salary->salary;
  rankMin = 0;
  rankMax = -1;
  traverseTree( salaryRoot, mySalary, rankMin, rankMax);
  return true;
}
//************************************************************************************************************************************************

void CPersonalAgenda::deleteEmpNode( Employee * nodeToDelete )
{
  if (nodeToDelete->left == nullptr && nodeToDelete->right == nullptr) {
      if (nodeToDelete == empRoot)
          empRoot = nullptr; // node is the root, set root to null
      else {
        // node is not the root, set its parent's pointer to null
        if (nodeToDelete->parent->left == nodeToDelete)
            nodeToDelete->parent->left = nullptr;
        else
            nodeToDelete->parent->right = nullptr;  
      }
      delete nodeToDelete;
      return;
  }
  // case 2: node has one child
  if (nodeToDelete->left == nullptr || nodeToDelete->right == nullptr) {
      Employee * child = nodeToDelete->left == nullptr ? nodeToDelete->right : nodeToDelete->left;
      if (nodeToDelete == empRoot){ // node is the root, update root's pointer
          empRoot = child;
          empRoot->parent = nullptr;
          empRoot->email->owner = empRoot;
          empRoot->salary->owner = empRoot;
          if( empRoot->right != nullptr)
              empRoot->right->parent = empRoot;
          if( empRoot->left != nullptr)
              empRoot->left->parent = empRoot;
      }  
      else {
        // node is not the root, update its parent's pointer
        child->parent = nodeToDelete->parent;
        if (nodeToDelete->parent->left == nodeToDelete)
            nodeToDelete->parent->left = child;
        else 
            nodeToDelete->parent->right = child;   
      }
      delete nodeToDelete;
      return;
  }
  // case 3: node has two children
  Employee * successor = nodeToDelete->right;
  while (successor->left != nullptr)
      successor = successor->left;

  nodeToDelete->name = successor->name;
  nodeToDelete->surname = successor->surname;
  nodeToDelete->email = successor->email;
  nodeToDelete->email->owner = nodeToDelete;
  nodeToDelete->salary = successor->salary;
  nodeToDelete->salary->owner = nodeToDelete;
  deleteEmpNode(successor);
}
//************************************************************************************************************************************************

void CPersonalAgenda::deleteEmailNode( Email * nodeToDelete )
{
  if (nodeToDelete->left == nullptr && nodeToDelete->right == nullptr) {
    if (nodeToDelete == emailRoot) {
        // node is the root, set root to null
        emailRoot = nullptr;
    } else {
        // node is not the root, set its parent's pointer to null
        if (nodeToDelete->parent->left == nodeToDelete) 
            nodeToDelete->parent->left = nullptr;
        else 
            nodeToDelete->parent->right = nullptr;
    }
    delete nodeToDelete;
    return;
  }
  // case 2: node has one child
  if (nodeToDelete->left == nullptr || nodeToDelete->right == nullptr) {
      Email * child = nodeToDelete->left == nullptr ? nodeToDelete->right : nodeToDelete->left;
      if (nodeToDelete == emailRoot){ // node is the root, update root's pointer
          emailRoot = child;
          emailRoot->parent = nullptr;
          emailRoot->owner->email = emailRoot;
          if( emailRoot->right != nullptr)
              emailRoot->right->parent = emailRoot;
          if( emailRoot->left != nullptr)
              emailRoot->left->parent = emailRoot;
      }
      else {
        // node is not the root, update its parent's pointer
        child->parent = nodeToDelete->parent;
        if (nodeToDelete->parent->left == nodeToDelete)
            nodeToDelete->parent->left = child;   
        else 
            nodeToDelete->parent->right = child;
      }
      delete nodeToDelete;
      return;
  }
  // case 3: node has two children
  Email * successor = nodeToDelete->right;
  while (successor->left != nullptr) 
      successor = successor->left;
  
  nodeToDelete->email = successor->email;
  nodeToDelete->owner = successor->owner;
  nodeToDelete->owner->email = nodeToDelete;
  deleteEmailNode(successor);
}
//************************************************************************************************************************************************

void CPersonalAgenda::deleteSalaryNode( Salary * nodeToDelete ){
    if (nodeToDelete->left == nullptr && nodeToDelete->right == nullptr) {
      if (nodeToDelete == salaryRoot) 
          salaryRoot = nullptr; // node is the root, set root to null
      else {
        // node is not the root, set its parent's pointer to null
        if (nodeToDelete->parent->left == nodeToDelete)
            nodeToDelete->parent->left = nullptr;
        else 
            nodeToDelete->parent->right = nullptr;
      }
      delete nodeToDelete;
      return;
  }
  // case 2: node has one child
  if (nodeToDelete->left == nullptr || nodeToDelete->right == nullptr) {
      Salary * child = nodeToDelete->left == nullptr ? nodeToDelete->right : nodeToDelete->left;
      if (nodeToDelete == salaryRoot){ // node is the root, update root's pointer
          salaryRoot = child;
          salaryRoot->parent = nullptr;
          salaryRoot->owner->salary = salaryRoot;
          if( salaryRoot->right != nullptr)
              salaryRoot->right->parent = salaryRoot;
          if( salaryRoot->left != nullptr)
              salaryRoot->left->parent = salaryRoot;
      }
      else {
        // node is not the root, update its parent's pointer
        child->parent = nodeToDelete->parent;
        if (nodeToDelete->parent->left == nodeToDelete)
            nodeToDelete->parent->left = child;
        else 
            nodeToDelete->parent->right = child;
      }
      delete nodeToDelete;
      return;
  }
  // case 3: node has two children
  Salary * successor = nodeToDelete->right;
  while (successor->left != nullptr) 
      successor = successor->left;
  
  nodeToDelete->salary = successor->salary;
  nodeToDelete->owner = successor->owner;
  nodeToDelete->owner->salary = nodeToDelete;
  deleteSalaryNode(successor);
}
//************************************************************************************************************************************************

bool CPersonalAgenda::del ( const string& name,const string& surname ){
  Employee * employeeToDelete = empRoot;
  Employee * parent = nullptr;
  if( !findByName(name, surname, employeeToDelete, parent) ) 
      return false;

  Email * emailToDelete = employeeToDelete->email;
  Salary * salaryToDelete = employeeToDelete->salary;

  deleteEmpNode( employeeToDelete );
  deleteEmailNode( emailToDelete );
  deleteSalaryNode( salaryToDelete );
  return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::del ( const string& email ){
  Email * emailToDelete = emailRoot;
  Email * parent = nullptr;
  if( ! findByEmail(email, emailToDelete, parent))
      return false;

  Employee * employeeToDelete = emailToDelete->owner;
  Salary * salaryToDelete = employeeToDelete->salary;

  deleteEmpNode( employeeToDelete );
  deleteEmailNode( emailToDelete );
  deleteSalaryNode( salaryToDelete );
  return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::changeName ( const string& email, const string& newName, const string& newSurname ){
  Email * emailPtr = emailRoot;
  Email * emailParent = nullptr;
  Employee * employeePtr = empRoot;
  Employee * empParent = nullptr;
  if( ! findByEmail(email, emailPtr, emailParent) || findByName(newName, newSurname, employeePtr, empParent))
      return false;
  
  unsigned int newSalary = emailPtr->owner->salary->salary;
  if( ! del(email) ) 
      return false;

  if( ! add( newName, newSurname, email, newSalary) )
      return false;

  return true;
}
//************************************************************************************************************************************************

bool CPersonalAgenda::changeEmail  ( const string& name, const string& surname, const string& newEmail ){
  Employee * employee = empRoot;
  Employee * empParent = nullptr;
  Email * email = emailRoot;
  Email * emailParent = nullptr;
  if( !findByName(name, surname, employee, empParent) || findByEmail(newEmail, email, emailParent) ) 
      return false;
  
  unsigned int newSalary = employee->salary->salary;
  if( ! del(name, surname) ) 
      return false;

  if( ! add( name, surname, newEmail, newSalary) )
      return false;

  return true;
}
//************************************************************************************************************************************************
//************************************************************************************************************************************************

#ifndef __PROGTEST__
int main ( void )
{
  string outName, outSurname;
  int lo, hi;
<<<<<<< HEAD

  CPersonalAgenda b0;
    assert ( b0 . add ( "John", "Smith", "john1", 30000 ) );
    assert ( b0 . add ( "Jane", "Doe", "jane1", 35000 ) );
    assert ( b0 . add ( "Bob", "Johnson", "bob0", 40000 ) );
    assert ( b0 . add ( "Emily", "Wang", "emily1", 25000 ) );
    assert ( b0 . add ( "Alex", "Kim", "alex1", 50000 ) );
    assert ( b0 . add ( "John", "Doe", "john2", 28000 ) );
    assert ( b0 . add ( "Jane", "Smith", "jane2", 32000 ) );
    assert ( b0 . add ( "Bob", "Miller", "bob2", 45000 ) );
    assert ( b0 . add ( "Emily", "Lee", "emily2", 27000 ) );
    assert ( b0 . add ( "Alex", "Chen", "alex2", 52000 ) );
    assert ( b0 . add ( "John", "Wang", "john3", 31000 ) );
    assert ( b0 . add ( "Jane", "Miller", "jane3", 38000 ) );
    assert ( b0 . add ( "Bob", "Chen", "bob3", 42000 ) );
    assert ( b0 . add ( "Emily", "Kim", "emily3", 26000 ) );
    assert ( b0 . add ( "Alex", "Doe", "alex3", 48000 ) );
    assert ( b0 . add ( "John", "Lee", "john4", 29000 ) );
    assert ( b0 . add ( "Jane", "Chen", "jane4", 36000 ) );
    assert ( b0 . add ( "Bob", "Kim", "bob4", 41000 ) );
    assert ( b0 . add ( "Emily", "Miller", "emily4", 24000 ) );
    assert ( b0 . add ( "Alex", "Wang", "alex4", 53000 ) );
    assert ( !b0 . add ( "John", "Smith", "john5", 29000 ) );
    assert ( !b0 . add ( "Jane", "Doe", "jane5", 32000 ) );
    assert ( !b0 . add ( "Bob", "Johnson", "bob5", 38000 ) );
    assert ( !b0 . add ( "Emily", "Wang", "emily5", 23000 ) );
    assert ( !b0 . add ( "Alex", "Kim", "alex5", 55000 ) );
    assert ( !b0 . add ( "John", "Doe", "john6", 31000 ) );
    assert ( !b0 . add ( "Jane", "Smith", "jane6", 34000 ) );
    assert ( !b0 . add ( "Bob", "Miller", "bob6", 42000 ) );
    assert ( !b0 . add ( "Emily", "Lee", "emily6", 25000 ) );
    assert ( !b0 . add ( "Alex", "Chen", "alex6", 56000 ) );


  CPersonalAgenda b0;
  assert ( b0 . add ( "Bedrich", "Smetana", "bob", 30000 ) );
  assert ( b0 . add ( "Antonin", "Dvorak", "doraka", 2000 ) );
  assert ( b0 . add ( "Klement", "Gottwald", "goti5", 69000 ) );
  assert ( b0 . add ( "Don", "Teflon", "dont", 25 ) );
  assert ( b0 . add ( "Alois", "Gottwald", "brrr", 45950 ) );
  assert ( b0 . add ( "pan", "zahrada", "zahra", 56458 ) );
  assert ( b0 . add ( "Alois", "jirasek", "jriia", 456 ) );
  assert ( b0 . add ( "petr", "zahrada", "kakakak", 22 ) );
  assert ( b0 . add ( "petr", "holy", "hol", 56478 ) );
  assert ( b0 . add ( "petr", "gocar", "oetrg", 60000 ) );
  assert ( b0 . add ( "Jan", "Gos", "rakrak", 30000 ) );
  assert ( b0 . add ( "jan", "zahrada", "brrrrbbbr", 69000 ) );
  assert ( b0 . add ( "pb", "zahrada", "ddddok", 45950 ) );
  assert ( b0 . add ( "pc", "zahrada", "dww", 56458 ) );
  assert ( b0 . add ( "zita", "zahrada", "jrgrgriia", 456 ) );
  assert ( b0 . add ( "zzita", "zahrada", "wdhuuwdh", 565478 ) );
  assert ( b0 . add ( "zzzita", "zahrada", "boooob", 30001 ) );
  assert ( b0 . add ( "karel", "zahrada", "booboob", 39001 ) );
  
  assert ( b0 . del ( "petr", "zahrada" ) );
  assert ( b0 . del ( "Klement", "Gottwald" ) );
  assert ( b0 . del ( "Don", "Teflon" ) );
  assert ( b0 . del ( "pan", "zahrada" ) );
  assert ( b0 . del ( "bob" ) );
  assert ( b0 . del ( "Antonin", "Dvorak" ) );
  assert ( b0 . del ( "jrgrgriia" ) );
  assert ( b0 . del ( "Alois", "Gottwald" ) );
  assert ( b0 . del ( "brrrrbbbr" ) );
  assert ( b0 . del ( "Alois", "jirasek" ) );
  assert ( b0 . del ( "booboob" ) );
  assert ( b0 . del ( "zzzita", "zahrada" ) );
  assert ( b0 . del ( "pb", "zahrada" ) );
  assert ( b0 . del ( "wdhuuwdh" ) );
  assert ( b0 . del ( "petr", "gocar" ) );
  assert ( b0 . del ( "Jan", "Gos" ) );
  assert ( b0 . del ( "dww" ) );
  assert ( b0 . del ( "petr", "holy" ) );

  CPersonalAgenda b1;
  assert ( b1 . add ( "John", "Smith", "john", 30000 ) );
  assert ( b1 . add ( "John", "Miller", "johnm", 35000 ) );
  assert ( b1 . add ( "Peter", "Smith", "peter", 23000 ) );
  assert ( ! b1 . add ( "John", "hoes", "johnm", 69420 ) );
  assert ( ! b1 . add ( "Peter", "Smith", "jozmrdhn", 69420 ) );
  assert ( b1 . getFirst ( outName, outSurname )
           && outName == "John"
           && outSurname == "Miller" );
  assert ( b1 . getNext ( "John", "Miller", outName, outSurname )
           && outName == "John"
           && outSurname == "Smith" );
  assert ( b1 . getNext ( "John", "Smith", outName, outSurname )
           && outName == "Peter"
           && outSurname == "Smith" );
  assert ( ! b1 . getNext ( "Peter", "Smith", outName, outSurname ) );
  assert ( b1 . setSalary ( "john", 32000 ) );
  assert ( b1 . getSalary ( "john" ) ==  32000 );
  assert ( b1 . getSalary ( "John", "Smith" ) ==  32000 );
  assert ( b1 . getRank ( "John", "Smith", lo, hi )
           && lo == 1
           && hi == 1 );
  assert ( b1 . getRank ( "Peter", "Smith", lo, hi ) );
  assert ( b1 . getRank ( "John", "Miller", lo, hi ) );
  assert ( b1 . getRank ( "john", lo, hi )
           && lo == 1
           && hi == 1 );
  assert ( b1 . getRank ( "peter", lo, hi )
           && lo == 0
           && hi == 0 );
  assert ( b1 . getRank ( "johnm", lo, hi )
           && lo == 2
           && hi == 2 );
  assert ( b1 . setSalary ( "John", "Smith", 35000 ) );
  assert ( b1 . getSalary ( "John", "Smith" ) ==  35000 );
  assert ( b1 . getSalary ( "john" ) ==  35000 );
  assert ( b1 . getRank ( "John", "Smith", lo, hi )
           && lo == 1
           && hi == 2 );
  assert ( b1 . getRank ( "john", lo, hi )
           && lo == 1
           && hi == 2 );
  assert ( b1 . getRank ( "peter", lo, hi )
           && lo == 0
           && hi == 0 );
  assert ( b1 . getRank ( "johnm", lo, hi )
           && lo == 1
           && hi == 2 );
  assert ( b1 . changeName ( "peter", "James", "Bond" ) );
  assert ( b1 . getSalary ( "peter" ) ==  23000 );
  assert ( b1 . getSalary ( "James", "Bond" ) ==  23000 );
  assert ( b1 . getSalary ( "Peter", "Smith" ) ==  0 );
  assert ( b1 . getFirst ( outName, outSurname )
           && outName == "James"
           && outSurname == "Bond" );
  assert ( b1 . getNext ( "James", "Bond", outName, outSurname )
           && outName == "John"
           && outSurname == "Miller" );
  assert ( b1 . getNext ( "John", "Miller", outName, outSurname )
           && outName == "John"
           && outSurname == "Smith" ); 
  assert ( ! b1 . getNext ( "John", "Smith", outName, outSurname ) );
  assert ( b1 . changeEmail ( "James", "Bond", "james" ) );
  assert ( b1 . getSalary ( "James", "Bond" ) ==  23000 );
  assert ( b1 . getSalary ( "james" ) ==  23000 );
  assert ( b1 . getSalary ( "peter" ) ==  0 );
  assert ( b1 . del ( "james" ) );
  assert ( b1 . getRank ( "john", lo, hi )
           && lo == 0
           && hi == 1 );
  assert ( b1 . del ( "John", "Miller" ) );
  assert ( b1 . getRank ( "john", lo, hi )
           && lo == 0
           && hi == 0 );           
  assert ( b1 . getFirst ( outName, outSurname )
           && outName == "John"
           && outSurname == "Smith" );
  assert ( ! b1 . getNext ( "John", "Smith", outName, outSurname ) );
  assert ( b1 . del ( "john" ) );
  // Agenda empty
  assert ( ! b1 . del ( "john" ) );
  assert ( ! b1 . del ( "John", "Smith" ) );
  assert ( ! b1 . changeName ( "peter", "James", "Bond" ) );
  assert ( b1 . getSalary ( "peter" ) ==  0 );
  assert ( ! b1 . getNext ( "James", "Bond", outName, outSurname ) );
  assert ( ! b1 . changeEmail ( "James", "Bond", "james" ) );
  assert ( ! b1 . getRank ( "john", lo, hi ) );
  assert ( ! b1 . getFirst ( outName, outSurname ) );
  assert ( b1 . add ( "John", "Smith", "john", 31000 ) );
  assert ( b1 . add ( "john", "Smith", "joHn", 31000 ) );
  assert ( b1 . add ( "John", "smith", "jOhn", 31000 ) );

  CPersonalAgenda b2;
  assert ( ! b2 . getFirst ( outName, outSurname ) );
  assert ( b2 . add ( "James", "Bond", "james", 70000 ) );
  assert ( b2 . add ( "James", "Smith", "james2", 30000 ) );
  assert ( b2 . add ( "Peter", "Smith", "peter", 40000 ) );
  assert ( ! b2 . add ( "James", "Bond", "james3", 60000 ) );
  assert ( ! b2 . add ( "Peter", "Bond", "peter", 50000 ) );
  assert ( ! b2 . changeName ( "joe", "Joe", "Black" ) );
  assert ( ! b2 . changeEmail ( "Joe", "Black", "joe" ) );
  assert ( ! b2 . setSalary ( "Joe", "Black", 90000 ) );
  assert ( ! b2 . setSalary ( "joe", 90000 ) );
  assert ( b2 . getSalary ( "Joe", "Black" ) ==  0 );
  assert ( b2 . getSalary ( "joe" ) ==  0 );
  assert ( ! b2 . getRank ( "Joe", "Black", lo, hi ) );
  assert ( ! b2 . getRank ( "joe", lo, hi ) );
  assert ( ! b2 . changeName ( "joe", "Joe", "Black" ) );
  assert ( ! b2 . changeEmail ( "Joe", "Black", "joe" ) );
  assert ( ! b2 . del ( "Joe", "Black" ) );
  assert ( ! b2 . del ( "joe" ) );
  assert ( ! b2 . changeName ( "james2", "James", "Bond" ) );
  assert ( ! b2 . changeEmail ( "Peter", "Smith", "james" ) );
  assert ( ! b2 . changeName ( "peter", "Peter", "Smith" ) );
  assert ( ! b2 . changeEmail ( "Peter", "Smith", "peter" ) );
  assert ( b2 . del ( "Peter", "Smith" ) );
  assert ( ! b2 . changeEmail ( "Peter", "Smith", "peter2" ) );
  assert ( ! b2 . setSalary ( "Peter", "Smith", 35000 ) );
  assert ( b2 . getSalary ( "Peter", "Smith" ) ==  0 );
  assert ( ! b2 . getRank ( "Peter", "Smith", lo, hi ) );
  assert ( ! b2 . changeName ( "peter", "Peter", "Falcon" ) );
  assert ( ! b2 . setSalary ( "peter", 37000 ) );
  assert ( b2 . getSalary ( "peter" ) ==  0 );
  assert ( ! b2 . getRank ( "peter", lo, hi ) );
  assert ( ! b2 . del ( "Peter", "Smith" ) );
  assert ( ! b2 . del ( "peter" ) );
  assert ( b2 . add ( "Peter", "Smith", "peter", 40000 ) );
  assert ( b2 . getSalary ( "peter" ) ==  40000 );

  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
