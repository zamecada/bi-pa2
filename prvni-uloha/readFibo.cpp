#include <iostream>
#include <fstream>

using namespace std;

int main() {
    ifstream file("example/src_8.fib", ios::binary);

    if (!file) {
        cerr << "Error: unable to open file" << endl;
        return 1;
    }

    const int bufferSize = 1024;
    unsigned char buffer[bufferSize];
    int bufferIndex = 0;

    unsigned char byte;
    bool hasAdjacentOnes = false;
    while (file.read(reinterpret_cast<char*>(&byte), sizeof(byte))) {
        buffer[bufferIndex++] = byte;

        bool lastBit = byte & 1;
        bool hasOnes = false;
        for (int i = 1; i < 8; ++i) {
            bool bit = byte & (1 << i);
            if (bit && lastBit) {
                hasAdjacentOnes = true;
                break;
            }
            lastBit = bit;
            hasOnes |= bit;
        }

        if (hasAdjacentOnes) {
            printf("calling function\n");
            // Call another function here to process the buffer
            //processBuffer(buffer, bufferIndex);
            bufferIndex = 0;
            hasAdjacentOnes = false;
        }
    }

    if (bufferIndex > 0) {
        printf("calling function\n");
            // Call another function here to process the buffer
        //processBuffer(buffer, bufferIndex);
    }

    file.close();
    return 0;
}

