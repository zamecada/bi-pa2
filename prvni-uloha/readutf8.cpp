#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

vector<unsigned char> readBinaryFile(const string& filename) {
    ifstream input(filename, ios::binary);
    if (!input) {
        cerr << "Failed to open file: " << filename << endl;
        exit(1);
    }

    input.seekg(0, ios::end);
    size_t fileSize = input.tellg();
    input.seekg(0, ios::beg);

    vector<unsigned char> buffer(fileSize);
    input.read(reinterpret_cast<char*>(&buffer[0]), fileSize);

    return buffer;
}

void printDecimalUTF8(const vector<unsigned char>& buffer) {
    size_t i = 0;
    while (i < buffer.size()) {
        unsigned char c = buffer[i];
        unsigned int codePoint;

        if (c <= 0x7f) {
            codePoint = c;
            i += 1;
        } else if (c <= 0xdf && i + 1 < buffer.size()) {
            codePoint = ((c & 0x1f) << 6) | (buffer[i + 1] & 0x3f);
            i += 2;
        } else if (c <= 0xef && i + 2 < buffer.size()) {
            codePoint = ((c & 0x0f) << 12) | ((buffer[i + 1] & 0x3f) << 6) | (buffer[i + 2] & 0x3f);
            i += 3;
        } else if (c <= 0xf7 && i + 3 < buffer.size()) {
            codePoint = ((c & 0x07) << 18) | ((buffer[i + 1] & 0x3f) << 12) | ((buffer[i + 2] & 0x3f) << 6) | (buffer[i + 3] & 0x3f);
            i += 4;
        } else {
            cerr << "Invalid UTF-8 sequence" << endl;
            exit(1);
        }
        cout << codePoint << " bob ";
    }
    cout << endl;
}

int main() {
    vector<unsigned char> buffer = readBinaryFile("example/src_3.utf8");
    printDecimalUTF8(buffer);

    return 0;
}
