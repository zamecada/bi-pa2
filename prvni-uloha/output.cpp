#include <iostream>
#include <fstream>

using namespace std;

int main() {
    ofstream output("easy.bin", ios::binary);

    bool bits[] = {0,0,0,0,0,0,0,1};

    unsigned char buffer = 0;
    int count = 0;

    for (int i = 7; i >= 0; i--) {
        buffer <<= 1;
        buffer |= bits[i];
        count++;

        if (count == 8) {
            output.put(buffer);
            buffer = 0;
            count = 0;
        }
    }

    if (count > 0) {
        buffer <<= (8 - count);
        output.put(buffer);
    }

    bool bits2[] = {1,0,0,0,0,0,0,0};

    buffer = 0;
    count = 0;

    for (int i = 7; i >= 0; i--) {
        buffer <<= 1;
        buffer |= bits2[i];
        count++;

        if (count == 8) {
            output.put(buffer);
            buffer = 0;
            count = 0;
        }
    }

    if (count > 0) {
        buffer <<= (8 - count);
        output.put(buffer);
    }

    output.close();

    return 0;
}
