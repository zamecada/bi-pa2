#include <iostream>
#include <fstream>

using namespace std;

int main (){
    ofstream file1 ("easy.bin", ios::binary);

    if( !file1.is_open() ) {
        cout << "Thats a problem my fella" << endl;
        return 1;
    }

    string arr = { 0x80, 0x1 };

    file1.write( reinterpret_cast <char*> (arr), sizeof(arr));

    file1.close();

    return 0;
}