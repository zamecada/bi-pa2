#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;
#endif /* __PROGTEST__ */

vector<int> fibo;
bool bits[8];
size_t bitCount = 0;

void createFiboSequence(int max, int a = 0, int b = 1) 
{
  int c = a + b;
  if (c >= max && c != 1)
    return;
  
  fibo.push_back(c);
  createFiboSequence( max, b, c );
}

void writeInFile( ofstream & output ) 
{
  unsigned char buffer = 0;
  int count = 0;

  for (int i = 7; i >= 0; i--) {
      buffer <<= 1;
      buffer |= bits[i];
      cerr << bits[i] << " ";
      count++;

      if (count == 8) {
          output.put(buffer);
          buffer = 0;
          count = 0;
      }
  }
  cerr << endl;

  if (count > 0) {
      buffer <<= (8 - count);
      output.put(buffer);
  }
  bitCount = 0;
  memset(bits, 0, sizeof(bits));
}

void printFiboCode(string & position, ofstream & output) 
{
  int j = 1;

  for( int i = position.size() - 1 ; i >= 0 ; i--){
    for( ; j != (int)position[i]; j++ ){
      bits[ bitCount++ ] = 0;
      if(bitCount == 8)
        writeInFile ( output );
    }
    bits[ bitCount++ ] = 1;
    if(bitCount == 8)
      writeInFile ( output );
    j++;
  }
  bits[ bitCount++ ] = 1;
}


bool getFiboElements(int number, ofstream & output ) 
{
  if (fibo.size() <= 2) {
    fibo.clear();
    createFiboSequence(number);
  } else if (fibo.back() < number) {
    createFiboSequence(number, fibo[fibo.size() - 2], fibo.back() );
  }
  bool myNumber[1024];
  int count = 0;
  int i = fibo.size() - 1;
  while( fibo[i] > number )
    i--;

  myNumber[count++] = 1; //ukoncovaci jednicka
  //cerr << "consists of ";
  for( ; i >= 0; i-- ) {
    if (fibo[i] <= number) {
      myNumber[count++] = 1;
      //cerr << fibo [i] << " ";
      number -= fibo[i];
    } 
    else myNumber[count++] = 0;
  }
  while (count > 0) {
    bits[bitCount++] = myNumber[--count];
    if(bitCount == 8)
      writeInFile( output );
  }
  return true;
}

bool is10xxxxxx(unsigned char byte) {
    return (byte & 0xC0) == 0x80;
}

bool utf8ToFibonacci ( const char * inFile, const char * outFile )
{
  ifstream input(inFile, ios::binary);
    if (!input)
        return false;

  input.seekg(0, ios::end);
  size_t fileSize = input.tellg();
  input.seekg(0, ios::beg);

  vector<unsigned char> buffer(fileSize);
  input.read(reinterpret_cast<char*>(&buffer[0]), fileSize);

  ofstream output(outFile, ios::binary);
  if (!output)
      return false;

  size_t i = 0;
  while (i < buffer.size()) {
      unsigned char c = buffer[i];
      unsigned int number;

      if (c <= 0x7f) {
          //cerr << "One";
          number = c;
          i += 1;
      } else if (c <= 0xdf && i + 1 < buffer.size()) {
          if( ! is10xxxxxx( buffer[i+1] ) ) 
            return false;
          //cerr << "Two";
          number = ((c & 0x1f) << 6) | (buffer[i + 1] & 0x3f);
          i += 2;
      } else if (c <= 0xef && i + 2 < buffer.size()) {
          if( ! is10xxxxxx( buffer[i+1] ) || ! is10xxxxxx( buffer[i+2] ) ) 
            return false;
          //cerr << "Three";
          number = ((c & 0x0f) << 12) | ((buffer[i + 1] & 0x3f) << 6) | (buffer[i + 2] & 0x3f);
          i += 3;
      } else if (c <= 0xf7 && i + 3 < buffer.size()) {
          if( ! is10xxxxxx( buffer[i+1] ) || ! is10xxxxxx( buffer[i+2] ) || ! is10xxxxxx( buffer[i+3] ))  
            return false;
          //cerr << "Four";
          number = ((c & 0x07) << 18) | ((buffer[i + 1] & 0x3f) << 12) | ((buffer[i + 2] & 0x3f) << 6) | (buffer[i + 3] & 0x3f);
          i += 4;
      } else 
          return false;

      if(number > 1114111) //Higher than 0x10ffff
        return false;

      cerr << endl << number+1 << endl;
      getFiboElements(number + 1, output );
  }
  input.close();

  if(bitCount != 0)
    writeInFile( output );
  
  output.close();
  
  return true;
}

// UTF8 TO FIBONACCI ENDED
/************************************************************************************************************************************/
// FIBONACCI TO UTF8 STARTED

void getFiboSequence(int size) 
{
  int a = 0, b = 1, tmp;
  fibo.clear();

  for(int i = 0; i < size; i++){
    tmp = a + b;
    fibo.push_back(tmp);
    a = b;
    b = tmp;
  }
}

bool writeIntAsUTF8(int num, ofstream & out) {
                //Higher than 0x10ffff
  if (num < 0 || num > 1114111) { 
      return false;
  }
  if (num <= 0x7F) {
      out.put(num); // 1-byte encoding
  } else if (num <= 0x7FF) {
      out.put(((num >> 6) & 0x1F) | 0xC0); // 2-byte encoding
      out.put((num & 0x3F) | 0x80);
  } else if (num <= 0xFFFF) {
      out.put(((num >> 12) & 0x0F) | 0xE0); // 3-byte encoding
      out.put(((num >> 6) & 0x3F) | 0x80);
      out.put((num & 0x3F) | 0x80);
  } else if (num <= 0x10FFFF) {
      out.put(((num >> 18) & 0x07) | 0xF0); // 4-byte encoding
      out.put(((num >> 12) & 0x3F) | 0x80);
      out.put(((num >> 6) & 0x3F) | 0x80);
      out.put((num & 0x3F) | 0x80);
  } else 
      return false;
      
  return true;
}

int convertToDecimal( const bool (& number)[8192], int & size ){
  if( size > (int)fibo.size())
    getFiboSequence(size);

  int decNum = 0;
  for( int i = 0; i < size; i++)
    if( number[i] == 1 )
      decNum += fibo[i];
  
  decNum -= 1;
  size = 0;
  return decNum;
}

bool getNumber( unsigned char (&buffer)[1024], int & bufferIndex, int & bitIndex, ofstream & output)
{
  bool number[8192];
  for(int i = 0; i < bufferIndex; i++){
    for (int j = 0; j < 8; j++) {
      number[bitIndex++] = buffer[i] & 1;
      buffer[i] >>= 1;
      if(number[bitIndex-1] == 1 && number[bitIndex-2] == 1 ){
        if( ! writeIntAsUTF8( convertToDecimal(number, --bitIndex ) , output ) )
          return false;
      }
    }
  }
  return true;
}

bool fibonacciToUtf8 (const char * inFile, const char * outFile )
{
  ifstream input(inFile, ios::binary);
  if (!input) 
      return false;
  
  const int bufferSize = 1024;
  unsigned char buffer[bufferSize];
  int bufferIndex = 0, bitIndex = 0;
  unsigned char byte;
  bool hasAdjacentOnes = false;

  ofstream output(outFile, ios::binary);
  if (!output)
    return false;

  size_t howManyOnes = 0;
  bool lastBit = byte & 1;


  while (input.read(reinterpret_cast<char*>(&byte), sizeof(byte))) {
      buffer[bufferIndex++] = byte;

      bool hasOnes = false;
      
      for (int i = 0; i < 8; ++i) {
          bool bit = byte & (1 << i);
          if(bit == true){
            if(lastBit == false) 
              howManyOnes = 1;
            else 
              howManyOnes += 1;
          }            
          if (bit && lastBit) {
              hasAdjacentOnes = true;
              break;
          }
          lastBit = bit;
          hasOnes |= bit;
      }

      if (hasAdjacentOnes) {
          if( ! getNumber(buffer, bufferIndex, bitIndex, output) ) 
            return false;
          bufferIndex = 0;
          hasAdjacentOnes = false;
          lastBit = false;
      }
  }
  if (bufferIndex > 0){
    if(howManyOnes != 2)
      return false;
    else 
      if( ! getNumber(buffer, bufferIndex, bitIndex, output) )
        return false;
  }
  input.close();

return true;
}
  
#ifndef __PROGTEST__

bool compareStreams(ifstream& file1, ifstream& file2) {
    char ch1, ch2;

    while (file1.get(ch1) && file2.get(ch2)) {
        if (ch1 != ch2) {
            return false;
        }
    }

    return true;
}

bool identicalFiles(const char* file1, const char* file2) {
    ifstream file1Stream(file1, ios::binary);
    ifstream file2Stream(file2, ios::binary);

    if (!file1Stream || !file2Stream) {
        return false;
    }

    if (!compareStreams(file1Stream, file2Stream)) {
        file1Stream.close();
        file2Stream.close();
        return false;
    }

    file1Stream.close();
    file2Stream.close();
    return true;
}

int main ( int argc, char * argv [] )
{   
  /*assert ( identicalFiles( "areIdentical/identical1.bin", "areIdentical/identical2.bin") );
  assert ( ! identicalFiles( "areIdentical/identical1.bin", "areIdentical/different.bin") );

  assert ( utf8ToFibonacci ( "example/src_0.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_0.fib" ) );
  assert ( utf8ToFibonacci ( "example/src_1.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_1.fib" ) );
  assert ( utf8ToFibonacci ( "example/src_2.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_2.fib" ) );
  assert ( utf8ToFibonacci ( "example/src_3.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_3.fib" ) );
  assert ( utf8ToFibonacci ( "example/src_4.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_4.fib" ) );
  assert ( ! utf8ToFibonacci ( "example/src_5.utf8", "output.fib" ) );
  
  assert ( fibonacciToUtf8 ( "example/src_6.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_6.utf8" ) );
  assert ( fibonacciToUtf8 ( "example/src_7.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_7.utf8" ) );
  assert ( fibonacciToUtf8 ( "example/src_8.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_8.utf8" ) );
  assert ( fibonacciToUtf8 ( "example/src_9.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_9.utf8" ) );
  assert ( fibonacciToUtf8 ( "example/src_10.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_10.utf8" ) );

  assert ( ! fibonacciToUtf8 ( "example/src_11.fib", "output.utf8" ) );

  assert ( !utf8ToFibonacci( "in_5057133.bin", "output.fib"));

  utf8ToFibonacci ( "in_napoveda2.bin", "output.fib");*/
 
  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
