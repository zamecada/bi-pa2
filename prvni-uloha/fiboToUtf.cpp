#include <iostream>
#include <fstream>

using namespace std;

const int BITS_IN_BYTE = 8;

bool getBit(unsigned char byte, int bitIndex) {
  return (byte >> bitIndex) & 1;
}

bool getNumber(unsigned char *buffer, int& bufferIndex, int& bitIndex, unsigned char& output) {
  output = 0;
  int bitCount = 0;
  while (bitCount < 8) {
    if (getBit(buffer[bufferIndex], bitIndex)) {
      output |= (1 << bitCount);
    }
    bitIndex++;
    if (bitIndex == BITS_IN_BYTE) {
      bitIndex = 0;
      bufferIndex++;
    }
    bitCount++;
  }
  return true;
}

bool decode(ifstream& inFile, ofstream& outFile) {
  unsigned char buffer[BITS_IN_BYTE];
  unsigned char outputBuffer[BITS_IN_BYTE];
  int bufferIndex = 0;
  int bitIndex = 0;
  int outputBufferIndex = 0;

  while (inFile.read((char*)buffer, BITS_IN_BYTE)) {
    if (!getNumber(buffer, bufferIndex, bitIndex, outputBuffer[outputBufferIndex])) {
      return false;
    }
    outputBufferIndex++;
    if (outputBufferIndex == BITS_IN_BYTE) {
      outFile.write((char*)outputBuffer, BITS_IN_BYTE);
      outputBufferIndex = 0;
    }
  }

  if (bufferIndex > 0) {
    if (!getNumber(buffer, bufferIndex, bitIndex, outputBuffer[outputBufferIndex])) {
      return false;
    }
    outputBufferIndex++;
    if (outputBufferIndex <= BITS_IN_BYTE) {
      outFile.write((char*)outputBuffer, outputBufferIndex);
    }
  }

  return true;
}

int main() {
  ifstream inFile("example/src_8.fib", ios::binary);
  ofstream outFile("output.utf8", ios::binary);
  if (!decode(inFile, outFile)) {
    cerr << "Decoding failed." << endl;
  }
  inFile.close();
  outFile.close();
  return 0;
}
