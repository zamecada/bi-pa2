#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <ctime>
#include <climits>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <stdexcept>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */

// uncomment if your code implements initializer lists
// #define EXTENDED_SYNTAX

class CRangeList;

class CRange
{
  public:
    CRange( long long low, long long high ) : lo( low ), hi( high ) {
      if ( lo > hi )
        throw logic_error("Invalid exception thrown");
    }
    friend class CRangeList;
    friend CRangeList operator+(const CRange& r1, const CRange& r2);
    friend CRangeList operator-(const CRange& r1, const CRange& r2);
    friend ostream & operator << ( ostream & out, const CRangeList & list);
  private:
    long long lo;
    long long hi;
};
class CRangeList
{
  public:
    CRangeList() : list() {}

    bool includes ( const CRange & element ) const {
      for( const auto & interval : list ){
        if ( element.lo >= interval.lo && element.hi <= interval.hi )
            return true;
      }
      return false;
    }

    bool includes ( long long num ) const {
      for( const auto & interval : list ){
        if ( num >= interval.lo && num <= interval.hi )
            return true;
      }
      return false;
    }

    CRangeList & operator + ( const CRange& r2 ) {
        *this += r2;
        return *this;
    }

    CRangeList & operator - ( const CRange& r2 ) {
        *this -= r2;
        return *this;
    }
    
    CRangeList & operator += (const CRange& newInterval) {
        // If the list is empty, simply add the new interval
        if (list.empty()) {
            list.push_back(newInterval);
            return *this;
        }

        // Iterate over the list and merge any overlapping intervals
        auto it = list.begin();
        while (it != list.end()) {
            if ( newInterval.hi < it->lo - 1 && it->lo != LLONG_MIN ) {
                // If the new interval is before the current interval, insert it
                list.insert(it, newInterval);
                return *this;
            } else if (newInterval.lo > it->hi + 1 && it->hi != LLONG_MAX ) {
                // If the new interval is after the current interval, continue iterating
                ++it;
            } else {
                // If the new interval overlaps with the current interval, merge them
                it->lo = min(it->lo, newInterval.lo);
                it->hi = max(it->hi, newInterval.hi);
                // Merge any subsequent intervals that overlap with the merged interval
                auto nextIt = it;
                ++nextIt;
                while (nextIt != list.end() && (nextIt->lo <= it->hi + 1 || it->hi == LLONG_MAX ) ) {
                    it->hi = max(it->hi, nextIt->hi);
                    nextIt = list.erase(nextIt);
                }
                return *this;
            }
        }

        // If the new interval is after all intervals, insert it at the end
        list.push_back(newInterval);
        return *this;
    }

    CRangeList & operator += (const CRangeList & other) {
      // Iterate over the intervals in the other list and insert them into this list
      for (const auto & interval : other.list) {
          *this += interval;
      }
      return *this;
    }

    CRangeList & operator -= ( const CRange & element ){
        vector<CRange> newList;
        for (const auto & interval : list) {
            // Interval to delete is inside an existing interval
            if (interval.lo <= element.lo && interval.hi >= element.hi) {
                if (interval.lo < element.lo) {
                    newList.push_back(CRange(interval.lo, element.lo - 1));
                }
                if (interval.hi > element.hi) {
                    newList.push_back(CRange(element.hi + 1, interval.hi));
                }
            }
            // Interval to delete is out of bounds of current interval
            else if (interval.hi < element.lo || interval.lo > element.hi) {
                newList.push_back(interval);
            }
            // Interval to delete covers part of current interval but also goes further than that
            else if (interval.lo < element.lo) {
                newList.push_back(CRange(interval.lo, element.lo - 1));
            }
            // Interval to delete covers part of current interval and came from previous part
            else if (interval.hi > element.hi) {
                newList.push_back(CRange(element.hi + 1, interval.hi));
            }
        }

        if( newList.size() == 0)
            list.clear();
        else
            list = newList;

        return *this;
    }

    CRangeList & operator -= ( const CRangeList & other ) {
        for (const auto & interval : other.list) {
        *this -= interval;
    }
    return *this;
    }

    CRangeList & operator = ( const CRange & element ) {
        list.clear();  // Clear the current list
        list.push_back(element);  // Add the new element
        return *this;
    }
    
    CRangeList & operator = ( const CRangeList & other ) {
        CRangeList temp; // create a temporary object
        for (const auto& interval : other.list) 
            temp += interval; // add each interval to the temporary object
        
        list = temp.list; // assign the temporary object's list to the current object's list
        return *this;
    }
    
    bool operator == ( const CRangeList & other ) const {
        if (this == &other ) 
            return true;
        
        if( (other.list.size() != this->list.size()) )
            return false;
        
        for (size_t i = 0; i < other.list.size(); ++i) 
            if (other.list[i].lo != this->list[i].lo || other.list[i].hi != this->list[i].hi)
                return false;

        return true;
    } 

    bool operator != ( const CRangeList & other ) const{
        return !(*this == other);
    }

    friend ostream & operator <<( ostream & out, const CRangeList & list);
  private:
    vector <CRange> list;
};

CRangeList operator + (const CRange & r1, const CRange & r2) {
    CRangeList result;
    result += r1;
    result += r2;
    return result;
}

CRangeList operator - (const CRange& r1, const CRange& r2) {
    CRangeList result;
    result += r1;
    result -= r2;
    return result;
}

ostream & operator << ( ostream & out, const CRangeList & list) {
    stringstream ss;
    ss << "{";
    for (const auto & interval : list.list) {
        if (interval.lo == interval.hi) {
            ss << interval.lo;
        } else {
            ss << "<" << interval.lo << ".." << interval.hi << ">";
        }
        ss << ",";
    }
    // remove the last comma
    string str = ss.str();
    if (str.back() == ',') {
        str.pop_back();
    }
    str += "}";
    out << str;
    return out;
}

#ifndef __PROGTEST__
string toString ( const CRangeList & x )
{
  ostringstream oss;
  oss << x;
  // cout << oss.str() << endl;
  return oss . str ();
}

int main ( void )
{
    CRangeList c;
    
    // napoveda
    // cout << "Expected output: {<-9223372036854775807..9223372036854775807>}" << endl;
    // cout << "Actual output: {9223372036854775807,<-9223372036854775807..9223372036854775806>}\n" << endl;

    // v +=  operatoru se opiram nekdy o ( hi + 1 ), pripadne o ( lo - 1 ) a pokud je hi LLONG_MAX, rep. lo LLONG_MIN, tak je pruser

    c += CRange ( 8, 8 );
    toString ( c );
    c += CRange ( 6, 7 );
    toString ( c );

    c += CRange ( LLONG_MAX, LLONG_MAX );
    toString ( c );
    c += CRange ( LLONG_MIN + 1 , LLONG_MAX - 1);
    toString ( c );
    c = CRange ( LLONG_MAX, LLONG_MAX ) + CRange ( LLONG_MIN + 1 , LLONG_MAX - 1);
    toString ( c );

    c = CRange ( LLONG_MAX, LLONG_MAX );
    c += CRange ( LLONG_MIN, LLONG_MAX );
    
    toString ( c );


    // cout << "-\n-\n-\n-\n-\n-" << endl;

  CRangeList a, b;

  assert ( sizeof ( CRange ) <= 2 * sizeof ( long long ) );
  a = CRange ( 5, 10 );
  a += CRange ( 25, 100 );
  assert ( toString ( a ) == "{<5..10>,<25..100>}" );
  a += CRange ( -5, 0 );
  a += CRange ( 8, 50 );
  assert ( toString ( a ) == "{<-5..0>,<5..100>}" );
  a += CRange ( 101, 105 ) + CRange ( 120, 150 ) + CRange ( 160, 180 ) + CRange ( 190, 210 );
  assert ( toString ( a ) == "{<-5..0>,<5..105>,<120..150>,<160..180>,<190..210>}" );
  a += CRange ( 106, 119 ) + CRange ( 152, 158 );
  assert ( toString ( a ) == "{<-5..0>,<5..150>,<152..158>,<160..180>,<190..210>}" );
  a += CRange ( -3, 170 );
  a += CRange ( -30, 1000 );
  assert ( toString ( a ) == "{<-30..1000>}" );
  b = CRange ( -500, -300 ) + CRange ( 2000, 3000 ) + CRange ( 700, 1001 );
  a += b;
  assert ( toString ( a ) == "{<-500..-300>,<-30..1001>,<2000..3000>}" );
  a -= CRange ( -400, -400 );
  assert ( toString ( a ) == "{<-500..-401>,<-399..-300>,<-30..1001>,<2000..3000>}" );
  a -= CRange ( 10, 20 ) + CRange ( 900, 2500 ) + CRange ( 30, 40 ) + CRange ( 10000, 20000 );
  assert ( toString ( a ) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}" );
  try
  {
    a += CRange ( 15, 18 ) + CRange ( 10, 0 ) + CRange ( 35, 38 );
    assert ( "Exception not thrown" == nullptr );
  }
  catch ( const std::logic_error & e )
  {
  }
  catch ( ... )
  {
    assert ( "Invalid exception thrown" == nullptr );
  }
  assert ( toString ( a ) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}" );
  b = a;
  assert ( a == b );
  assert ( !( a != b ) );
  b += CRange ( 2600, 2700 );
  assert ( toString ( b ) == "{<-500..-401>,<-399..-300>,<-30..9>,<21..29>,<41..899>,<2501..3000>}" );
  assert ( a == b );
  assert ( !( a != b ) );
  b += CRange ( 15, 15 );
  assert ( toString ( b ) == "{<-500..-401>,<-399..-300>,<-30..9>,15,<21..29>,<41..899>,<2501..3000>}" );
  assert ( !( a == b ) );
  assert ( a != b );
  assert ( b . includes ( 15 ) );
  assert ( b . includes ( 2900 ) );
  assert ( b . includes ( CRange ( 15, 15 ) ) );
  assert ( b . includes ( CRange ( -350, -350 ) ) );
  assert ( b . includes ( CRange ( 100, 200 ) ) );
  assert ( !b . includes ( CRange ( 800, 900 ) ) );
  assert ( !b . includes ( CRange ( -1000, -450 ) ) );
  assert ( !b . includes ( CRange ( 0, 500 ) ) );
  a += CRange ( -10000, 10000 ) + CRange ( 10000000, 1000000000 );
  assert ( toString ( a ) == "{<-10000..10000>,<10000000..1000000000>}" );
  b += a;
  assert ( toString ( b ) == "{<-10000..10000>,<10000000..1000000000>}" );
  b -= a;
  assert ( toString ( b ) == "{}" );
  b += CRange ( 0, 100 ) + CRange ( 200, 300 ) - CRange ( 150, 250 ) + CRange ( 160, 180 ) - CRange ( 170, 170 );
  assert ( toString ( b ) == "{<0..100>,<160..169>,<171..180>,<251..300>}" );
  b -= CRange ( 10, 90 ) - CRange ( 20, 30 ) - CRange ( 40, 50 ) - CRange ( 60, 90 ) + CRange ( 70, 80 );
  assert ( toString ( b ) == "{<0..9>,<20..30>,<40..50>,<60..69>,<81..100>,<160..169>,<171..180>,<251..300>}" );
#ifdef EXTENDED_SYNTAX
  CRangeList x { { 5, 20 }, { 150, 200 }, { -9, 12 }, { 48, 93 } };
  assert ( toString ( x ) == "{<-9..20>,<48..93>,<150..200>}" );
  ostringstream oss;
  oss << setfill ( '=' ) << hex << left;
  for ( const auto & v : x + CRange ( -100, -100 ) )
    oss << v << endl;
  oss << setw ( 10 ) << 1024;
  assert ( oss . str () == "-100\n<-9..20>\n<48..93>\n<150..200>\n400=======" );

  #endif /* EXTENDED_SYNTAX */
  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
