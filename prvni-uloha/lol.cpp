#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;
#endif /* __PROGTEST__ */

void function ( char & number ){
    printf("%u, %d, %c\n", number, number, number);
}

bool utf8ToFibonacci ( const char * inFile, const char * outFile )
{
    FILE *file = fopen( inFile , "rb"); // open binary file for reading

    if (file) {
        char byte;

        while (fread(&byte, 1, 1, file) == 1) {
            function( byte );
            if (byte == ' ') {
                // stop reading after whitespace
                break;
            }
        }

        fclose(file); // always remember to close the file
    }

  return true;
}

#ifndef __PROGTEST__

int main ( int argc, char * argv [] )
{
  utf8ToFibonacci( "example/src_0.utf8", nullptr);

  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
