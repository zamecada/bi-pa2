#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <deque>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <iterator>
#include <functional>
using namespace std;

class CDate
{
  public:
      CDate ( int y,
              int m,
              int d )
      : m_Year ( y ),
        m_Month ( m ),
        m_Day ( d )
    {
    }
    int compare ( const CDate & x ) const
    {
      if ( m_Year != x . m_Year )
        return m_Year - x . m_Year;
      if ( m_Month != x . m_Month )
        return m_Month - x . m_Month;
      return m_Day - x . m_Day;
    }
    int year ( void ) const
    {
      return m_Year;
    }
    //---------------------------------------------------------------------------------------------
    int month ( void ) const
    {
      return m_Month;
    }
    //---------------------------------------------------------------------------------------------
    int day ( void ) const
    {
      return m_Day;
    }
    //---------------------------------------------------------------------------------------------
    friend ostream & operator << ( ostream & os, const CDate & x )
    {
      char oldFill = os . fill ();
      return os << setfill ( '0' ) << setw ( 4 ) << x . m_Year << "-"
                                   << setw ( 2 ) << static_cast<int> ( x . m_Month ) << "-"
                                   << setw ( 2 ) << static_cast<int> ( x . m_Day )
                << setfill ( oldFill );
    }
    //---------------------------------------------------------------------------------------------
  private:
    int16_t m_Year;
    int8_t m_Month;
    int8_t m_Day;
};
#endif /* __PROGTEST__ */

class CInvoice
{
  public:
      CInvoice ( const CDate & date,
                 const string & seller,
                 const string & buyer,
                 unsigned int amount,
                 double vat )
      : m_Date(date),
        m_Seller(seller),
        m_Buyer(buyer),
        m_Amount(amount),
        m_VAT(vat)
    {
    }
    CDate date ( void ) const
    {
      return m_Date;
    }
    string buyer ( void ) const
    {
      return m_Buyer;
    }
    string seller ( void ) const
    {
      return m_Seller;
    }
    unsigned int amount ( void ) const
    {
      return m_Amount;
    }
    double vat ( void ) const
    {
      return m_VAT;
    }
    friend bool operator==(const CInvoice &a, const CInvoice &b)
    {
      return a.m_Date.compare(b.m_Date) == 0 &&
            a.m_Seller == b.m_Seller &&
            a.m_Buyer == b.m_Buyer &&
            a.m_Amount == b.m_Amount &&
            a.m_VAT == b.m_VAT;
    }
    friend class CVATRegister;
  private:
    CDate m_Date;
    string m_Seller;
    string m_Buyer;
    unsigned int m_Amount;
    double m_VAT;
};

class CSortOpt
{
  public:
    friend class CInvoiceCmp;
    static const int BY_DATE = 0;
    static const int BY_BUYER = 1;
    static const int BY_SELLER = 2;
    static const int BY_AMOUNT = 3;
    static const int BY_VAT = 4;
    CSortOpt ( void )
    {
        // Initialize sorting keys
        m_SortingKeys.clear();
    }
    CSortOpt & addKey ( int key, bool ascending = true )
    {
        // Add a sorting key
        m_SortingKeys.push_back({key, ascending});
        return *this;
    }
  private:
    vector<pair<int, bool>> m_SortingKeys;
};

class CInvoiceCmp
{
  public:
    CInvoiceCmp() : m_SortBy(CSortOpt()) {} // Add this constructor
    CInvoiceCmp(const CSortOpt &sortBy) : m_SortBy(sortBy) {}

    bool operator()(const CInvoice & a, const CInvoice & b) const
    {
      for (const auto &key : m_SortBy.m_SortingKeys)
      {
        int comparisonResult = 0;
        switch (key.first)
        {
        case CSortOpt::BY_DATE:
            comparisonResult = a.date().compare(b.date());
            break;
        case CSortOpt::BY_BUYER:
            comparisonResult = a.buyer().compare(b.buyer());
            break;
        case CSortOpt::BY_SELLER:
            comparisonResult = a.seller().compare(b.seller());
            break;
        case CSortOpt::BY_AMOUNT:
            comparisonResult = a.amount() < b.amount() ? -1 : (a.amount() > b.amount() ? 1 : 0);
            break;
        case CSortOpt::BY_VAT:
            comparisonResult = a.vat() < b.vat() ? -1 : (a.vat() > b.vat() ? 1 : 0);
            break;
        default:
            break;
        }

        if (comparisonResult != 0)
        {
            return key.second ? (comparisonResult < 0) : (comparisonResult > 0);
        }
      }

    return false;
    }

  private:
    const CSortOpt & m_SortBy;
};

string formatCompanyName(const string & name)
{
    string formattedName;
    bool isSpace = true; // flag to keep track of spaces
    for (unsigned long i = 0; i < name.length(); i++) {
        if (name[i] != ' ') {
            formattedName += name[i];
            isSpace = false;
        } else if (!isSpace && i != 0 && i != name.length()-1) {
            formattedName += ' ';
            isSpace = true;
        }
    }
    // remove space from beginning and end
    if (formattedName.length() > 0 && formattedName[0] == ' ') {
        formattedName.erase(0,1);
    }
    if (formattedName.length() > 0 && formattedName[formattedName.length()-1] == ' ') {
        formattedName.erase(formattedName.length()-1,1);
    }
    return formattedName;
}

class CVATRegister
{
  public:
    CVATRegister ( void ) {}
    CInvoice updateInvoice(const CInvoice &originalInvoice, const string &officialName, bool seller) const
    {
        return CInvoice(originalInvoice.date(),
                        seller ? officialName : originalInvoice.seller(),
                        seller ? originalInvoice.buyer() : officialName,
                        originalInvoice.amount(),
                        originalInvoice.vat());
    }
    bool registerCompany ( const string & name ){
        // Format name in the correct form (delete whitespaces)
        string formatted = formatCompanyName(name);
        // Check if the company is already registered
        if (m_Companies.find(formatted) != m_Companies.end())
        {
            return false;
        }

        // Register the company
        m_Companies.insert(formatted);
        return true;
    }

    bool caseInsensitiveCompare(const string &a, const string &b)
    {
        return equal(a.begin(), a.end(), b.begin(), b.end(),
                    [](char a, char b) { return tolower(a) == tolower(b); });
    }

    bool addIssued ( const CInvoice & x )
    {
        CInvoice bruh;
        CInvoice.m_Seller = formatCompanyName(x.seller());
        //x.m_Seller = formatCompanyName(x.seller());
        string buyer = formatCompanyName(x.buyer());

        // Check if the company exists and the seller and buyer are different
        if (!any_of(m_Companies.begin(), m_Companies.end(), [&](const string &company) { return caseInsensitiveCompare(seller, company); }) ||
            !any_of(m_Companies.begin(), m_Companies.end(), [&](const string &company) { return caseInsensitiveCompare(buyer, company); }) ||
            caseInsensitiveCompare(seller, buyer))
        {
          cout << "false1" << endl;
            return false;
        }
        cout << "My invoice:\n" << buyer << " " << seller << " " << x.amount() << " " << x.vat() << "\n\n" << endl;
        // Check if the invoice has not been added before
        for (const auto &invoice : m_Accepted[seller])
        {
          cout << "COMPARING:\n" << invoice.buyer() << " " << invoice.seller() << " " << invoice.amount() << " " << invoice.vat() << "\n"
               << x.buyer() << " " << x.seller() << " " << x.amount() << " " << x.vat() << endl;
            if (invoice == x)
            {
                return false;
            }
        }

        // Add the invoice to the issued list
        m_Issued[seller].insert(x);
        return true;
    }

    bool addAccepted(const CInvoice & x)
    {
        string seller = formatCompanyName(x.seller());
        string buyer = formatCompanyName(x.buyer());

        // Check if the company exists and the seller and buyer are different
        if (!any_of(m_Companies.begin(), m_Companies.end(), [&](const string &company) { return caseInsensitiveCompare(seller, company); }) ||
            !any_of(m_Companies.begin(), m_Companies.end(), [&](const string &company) { return caseInsensitiveCompare(buyer, company); }) ||
            caseInsensitiveCompare(seller, buyer))
        {
            return false;
        }

        // Check if the invoice has not been added before
        for (const auto &invoice : m_Accepted[buyer])
        {
            if (invoice == x)
            {
                return false;
            }
        }

        // Add the invoice to the accepted list
        m_Accepted[buyer].insert(x);
        return true;
    }

    bool delIssued(const CInvoice & x)
    {
        // Check if the company exists and the invoice is in the issued list
        auto company = m_Issued.find(x.seller());
        if (company == m_Issued.end())
        {
            return false;
        }

        auto invoice = company->second.find(x);
        if (invoice == company->second.end())
        {
            return false;
        }

        // Remove the invoice from the issued list
        company->second.erase(invoice);
        return true;
    }

    bool delAccepted(const CInvoice &x)
    {
        // Check if the company exists and the invoice is in the accepted list
        auto company = m_Accepted.find(x.buyer());
        if (company == m_Accepted.end())
        {
            return false;
        }

        auto invoice = company->second.find(x);
        if (invoice == company->second.end())
        {
            return false;
        }

        // Remove the invoice from the accepted list
        company->second.erase(invoice);
        return true;
    }

    list<CInvoice> unmatched(const string &company, const CSortOpt &sortBy) const
    {
        list<CInvoice> result;
        string formattedCompany = formatCompanyName(company);

        auto issuedCompany = m_Issued.find(formattedCompany);
        auto acceptedCompany = m_Accepted.find(formattedCompany);

        if (issuedCompany != m_Issued.end())
        {
            for (const auto &invoice : issuedCompany->second)
            {
                result.push_back(updateInvoice(invoice, company, true));
            }
        }

        if (acceptedCompany != m_Accepted.end())
        {
            for (const auto &invoice : acceptedCompany->second)
            {
                result.push_back(updateInvoice(invoice, company, false));
            }
        }

        // Sort the invoices using CInvoiceCmp and the sortBy object
        result.sort(CInvoiceCmp(sortBy));

        return result;
    }

  private:
    map<string, multiset<CInvoice, CInvoiceCmp>> m_Issued;
    map<string, multiset<CInvoice, CInvoiceCmp>> m_Accepted;
    set<string> m_Companies;
};

#ifndef __PROGTEST__
bool equalLists(const list<CInvoice> & a, const list<CInvoice> & b)
{
    // Check if the sizes of the two lists are equal
    if (a.size() != b.size())
    {
        return false;
    }

    // Compare the elements of the two lists
    return std::equal(a.begin(), a.end(), b.begin());
}

int main ( void )
{
  CVATRegister r;
  assert ( r . registerCompany ( "first Company" ) );
  assert ( r . registerCompany ( "Second     Company" ) );
  assert ( r . registerCompany ( "ThirdCompany, Ltd." ) );
  assert ( r . registerCompany ( "Third Company, Ltd." ) );
  assert ( !r . registerCompany ( "Third Company, Ltd." ) );
  assert ( !r . registerCompany ( " Third  Company,  Ltd.  " ) );
  assert ( r . addIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company ", 100, 20 ) ) );
  assert ( r . addIssued ( CInvoice ( CDate ( 2000, 1, 2 ), "FirSt Company", "Second Company ", 200, 30 ) ) );
  assert ( r . addIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company ", 100, 30 ) ) );
  assert ( r . addIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company ", 300, 30 ) ) );
  assert ( r . addIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", " Third  Company,  Ltd.   ", 200, 30 ) ) );
  assert ( r . addIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "Second Company ", "First Company", 300, 30 ) ) );
  assert ( r . addIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "Third  Company,  Ltd.", "  Second    COMPANY ", 400, 34 ) ) );
  assert ( !r . addIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company ", 300, 30 ) ) );
  assert ( !r . addIssued ( CInvoice ( CDate ( 2000, 1, 4 ), "First Company", "First   Company", 200, 30 ) ) );
  assert ( !r . addIssued ( CInvoice ( CDate ( 2000, 1, 4 ), "Another Company", "First   Company", 200, 30 ) ) );
  assert ( equalLists ( r . unmatched ( "First Company", CSortOpt () . addKey ( CSortOpt::BY_SELLER, true ) . addKey ( CSortOpt::BY_BUYER, false ) . addKey ( CSortOpt::BY_DATE, false ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 )
           } ) );
  assert ( equalLists ( r . unmatched ( "First Company", CSortOpt () . addKey ( CSortOpt::BY_DATE, true ) . addKey ( CSortOpt::BY_SELLER, true ) . addKey ( CSortOpt::BY_BUYER, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 )
           } ) );
  assert ( equalLists ( r . unmatched ( "First Company", CSortOpt () . addKey ( CSortOpt::BY_VAT, true ) . addKey ( CSortOpt::BY_AMOUNT, true ) . addKey ( CSortOpt::BY_DATE, true ) . addKey ( CSortOpt::BY_SELLER, true ) . addKey ( CSortOpt::BY_BUYER, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 )
           } ) );
  assert ( equalLists ( r . unmatched ( "First Company", CSortOpt () ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 )
           } ) );
  assert ( equalLists ( r . unmatched ( "second company", CSortOpt () . addKey ( CSortOpt::BY_DATE, false ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Third Company, Ltd.", "Second     Company", 400, 34.000000 )
           } ) );
  assert ( equalLists ( r . unmatched ( "last company", CSortOpt () . addKey ( CSortOpt::BY_VAT, true ) ),
           list<CInvoice>
           {
           } ) );
  assert ( r . addAccepted ( CInvoice ( CDate ( 2000, 1, 2 ), "First Company", "Second Company ", 200, 30 ) ) );
  assert ( r . addAccepted ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", " Third  Company,  Ltd.   ", 200, 30 ) ) );
  assert ( r . addAccepted ( CInvoice ( CDate ( 2000, 1, 1 ), "Second company ", "First Company", 300, 32 ) ) );
  assert ( equalLists ( r . unmatched ( "First Company", CSortOpt () . addKey ( CSortOpt::BY_SELLER, true ) . addKey ( CSortOpt::BY_BUYER, true ) . addKey ( CSortOpt::BY_DATE, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 32.000000 )
           } ) );
  assert ( !r . delIssued ( CInvoice ( CDate ( 2001, 1, 1 ), "First Company", "Second Company ", 200, 30 ) ) );
  assert ( !r . delIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "A First Company", "Second Company ", 200, 30 ) ) );
  assert ( !r . delIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Hand", 200, 30 ) ) );
  assert ( !r . delIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company", 1200, 30 ) ) );
  assert ( !r . delIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", "Second Company", 200, 130 ) ) );
  assert ( r . delIssued ( CInvoice ( CDate ( 2000, 1, 2 ), "First Company", "Second Company", 200, 30 ) ) );
  assert ( equalLists ( r . unmatched ( "First Company", CSortOpt () . addKey ( CSortOpt::BY_SELLER, true ) . addKey ( CSortOpt::BY_BUYER, true ) . addKey ( CSortOpt::BY_DATE, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 32.000000 )
           } ) );
  assert ( r . delAccepted ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", " Third  Company,  Ltd.   ", 200, 30 ) ) );
  assert ( equalLists ( r . unmatched ( "First Company", CSortOpt () . addKey ( CSortOpt::BY_SELLER, true ) . addKey ( CSortOpt::BY_BUYER, true ) . addKey ( CSortOpt::BY_DATE, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Third Company, Ltd.", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 32.000000 )
           } ) );
  assert ( r . delIssued ( CInvoice ( CDate ( 2000, 1, 1 ), "First Company", " Third  Company,  Ltd.   ", 200, 30 ) ) );
  assert ( equalLists ( r . unmatched ( "First Company", CSortOpt () . addKey ( CSortOpt::BY_SELLER, true ) . addKey ( CSortOpt::BY_BUYER, true ) . addKey ( CSortOpt::BY_DATE, true ) ),
           list<CInvoice>
           {
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 20.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 100, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "first Company", "Second     Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 2 ), "first Company", "Second     Company", 200, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 30.000000 ),
             CInvoice ( CDate ( 2000, 1, 1 ), "Second     Company", "first Company", 300, 32.000000 )
           } ) );
  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
