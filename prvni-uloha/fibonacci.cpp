#include <iostream>
#include <vector>

using namespace std;

vector<int> fib;

// Function to generate the Fibonacci sequence up to a certain limit
void generateFibonacci(int limit) {
    int a = 0, b = 1, c;
    while (a <= limit) {
        fib.push_back(a);
        c = a + b;
        a = b;
        b = c;
    }
}

// Function to represent a number as a sum of unique Fibonacci numbers
void uniqueFibonacciSum(int n) {
    vector<int> result;
    int i = fib.size() - 1;
    
    cout << n << " can be represented as the sum of the following Fibonacci numbers:\n";
    
    while (n > 0) {
        if (fib[i] <= n) {
            n -= fib[i];
            result.push_back(fib[i]);
        }
        i--;
    }

    for (int i = result.size() - 1; i >= 0; i--) {
        cout << result[i] << " ";
    }
}

int main() {
    int n;
    cout << "Enter a number: ";
    cin >> n;

    // Generate the Fibonacci sequence up to a certain limit
    generateFibonacci(n);

    // Represent the number as a sum of unique Fibonacci numbers
    uniqueFibonacciSum(n);

    return 0;
}